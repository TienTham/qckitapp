# README #

QckItApp - MasterChef Android project

### Purpose ###

* This project is under development
* Beta Version 1.0.1
* Version 1.1.1 (new)

### Setup ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### To DO ###

* Following things need to be improved:
      - Implementing pages layout with horizontal list view (on-going) 
      - Improve database implementation (on-going)
      - Releasing next version of database to enable more layout pager (on-going)
      - Customize Search method to get more friendly experience (scheduled)
      - Implementation of drawable and navigation bar (scheduled)

* Known bug:
      - File not supported when share to Whatsapp or Facebook message (Bug between android and Facebook sharing)
      - 
      - 

### Workload done ###

* To be validated:
      - Old actionTab need to be fully replaced by newer ToolBar
      - Embedding timer into project
      - Share action
      - Add to Favority in final page slide

### Developers ###

* tien.tominh@gmail.com (beginner android developer :D)
* QckIt team

![Screenshot_20170311-105258.png](https://bitbucket.org/repo/jqpyd8/images/2759243487-Screenshot_20170311-105258.png)

Reference:
- How to make README for bitbucket
https://bitbucket.org/tutorials/markdowndemo