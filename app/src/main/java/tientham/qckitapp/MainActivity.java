package tientham.qckitapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import tientham.qckitapp.database.RecipeSaver;
import tientham.qckitapp.ui.ShowList;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActionBar().hide();

        RecipeSaver recipeSaver = new RecipeSaver(this);
        try{
            copyDataBase("recipe_table.db");
            Log.d("copyDb: ", "dbCopied");
        }catch (IOException e){
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(getApplicationContext(), ShowList.class);
                startActivity(i);
                finish();
            }
        },1500);
    }

    private void copyDataBase(String dbName) throws IOException {
        // Open local database as the input stream
        InputStream myInput = getApplicationContext().getAssets().open(dbName);
        // Path to an empty database which has just created
        String outFileName = getDatabasePath(dbName).getPath();
        // Open the empty database as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);
        // Transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while((length = myInput.read(buffer)) > 0){
            myOutput.write(buffer, 0, length);
        }
        //Close the stream
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }
}
