package tientham.qckitapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.Vector;

import static tientham.qckitapp.model.RecipeCategory.GENERAL;

/**
 * Created by tien on 05/03/17.
 */

public class TabsPagerAdapter extends FragmentPagerAdapter{
    private Vector<Fragment> vectorList;

    public TabsPagerAdapter(FragmentManager fm, Vector<Fragment> v){
        super(fm);
        vectorList = v;
    }

    @Override
    public Fragment getItem(int arg0){
        switch (arg0){
            case 0:
                return vectorList.get(0);
            case 1:
                return vectorList.get(1);
            case 2:
                return vectorList.get(2);
            default:
                return null;
        }
        //return null;
    }

    @Override
    public int getCount(){
        return vectorList.size();
    }
}
