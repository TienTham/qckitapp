package tientham.qckitapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import tientham.qckitapp.model.Page;
import tientham.qckitapp.model.Recipe;

/**
 * Created by tien on 05/03/17.
 */

abstract class PageEntry implements BaseColumns{
    public static final String TABLE_NAME                   = "page";
    public static final String PAGE_COLUMN_NAME_PICTURE 	= "PICTURE";
    public static final String PAGE_COLUMN_NAME_TEXT 	    = "TEXT";
    public static final String PAGE_COLUMN_NAME_TIMEVAL 	= "TIMEVAL";
    public static final String PAGE_COLUMN_NAME_PAGEID	    = "PAGEID";
    public static final String PAGE_COLUMN_NAME_RECIPEID    = "RECIPEID";
}

abstract class RecipeEntry implements BaseColumns
{
    public static final String TABLE_NAME                       = "recipe";
    public static final String RECIPE_COLUMN_NAME_CATEGORY      = "CATEGORY";
    public static final String RECIPE_COLUMN_NAME_RECIPENAME    = "NAME";
}

public class PageReaderContract {

    private static PageReaderDbHelper mDbHelper = null;

    public PageReaderContract(Context context){
        if(mDbHelper == null){
            Log.d("mDbHelper: ", "null");
            mDbHelper = new PageReaderDbHelper(context);
            mDbHelper.onUpgrade(mDbHelper.getWritableDatabase(), 1, 1);
        }
    }
    private static final String TEXT_TYPE = " TEXT ";
    private static final String INTEGER_TYPE = " INTEGER ";
    private static final String COMMA_SEP = " , ";

    private static final String SQL_CREATE_PAGETABLE =
            "CREATE TABLE IF NOT EXISTS " + PageEntry.TABLE_NAME + " (" +
                    PageEntry._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    PageEntry.PAGE_COLUMN_NAME_PAGEID + INTEGER_TYPE + COMMA_SEP +
                    PageEntry.PAGE_COLUMN_NAME_PICTURE + TEXT_TYPE + COMMA_SEP +
                    PageEntry.PAGE_COLUMN_NAME_TEXT 	  + TEXT_TYPE + COMMA_SEP +
                    PageEntry.PAGE_COLUMN_NAME_TIMEVAL + INTEGER_TYPE + COMMA_SEP +
                    PageEntry.PAGE_COLUMN_NAME_RECIPEID + INTEGER_TYPE + COMMA_SEP +
                    "FOREIGN KEY(" + PageEntry.PAGE_COLUMN_NAME_RECIPEID + ")"
                    + " REFERENCES " + RecipeEntry.TABLE_NAME + "(" + RecipeEntry._ID + ")"
                    + " ON DELETE CASCADE " +
                    " )";
    private static final String SQL_CREATE_RECIPETABLE = "CREATE TABLE IF NOT EXISTS " + RecipeEntry.TABLE_NAME + " (" +
            RecipeEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
            RecipeEntry.RECIPE_COLUMN_NAME_CATEGORY + INTEGER_TYPE + COMMA_SEP +
            RecipeEntry.RECIPE_COLUMN_NAME_RECIPENAME + TEXT_TYPE +
            " )";

    private static final String SQL_DELETE_PAGETABLE = "DROP TABLE IF EXISTS " + PageEntry.TABLE_NAME;
    private static final String SQL_DELETE_RECIPETABLE = "DROP TABLE IF EXISTS " + RecipeEntry.TABLE_NAME;

    public class PageReaderDbHelper extends SQLiteOpenHelper
    {
        private static final int DATABASE_VERSION = 1;
        private static final String DATABASE_NAME = "recipe_table.db";

        PageReaderDbHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_RECIPETABLE);
            db.execSQL(SQL_CREATE_PAGETABLE);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            db.execSQL(SQL_DELETE_RECIPETABLE);
            db.execSQL(SQL_DELETE_PAGETABLE);
            onCreate(db);
        }
        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
            onUpgrade(db, oldVersion, newVersion);
        }
    }

    public void insert(Recipe recipe, boolean overwrite)
    {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String selection = RecipeEntry._ID + " = " + String.valueOf(recipe.getRecipeId());
        Cursor c = query(RecipeEntry.TABLE_NAME, selection);

        if(c.moveToFirst() && overwrite)
        {
            delete(RecipeEntry.TABLE_NAME, selection);
        }
        ContentValues values = new ContentValues();
        values.put(RecipeEntry.RECIPE_COLUMN_NAME_CATEGORY, recipe.getCategory().toInteger());
        values.put(RecipeEntry.RECIPE_COLUMN_NAME_RECIPENAME, recipe.getRecipeName());

        long recipeId = db.insert(RecipeEntry.TABLE_NAME, "null", values);


        Page[] pageArray = recipe.getPageArray();
        for(int pIdx=0; pIdx<pageArray.length; pIdx++)
        {
            values = new ContentValues();
            values.put(PageEntry.PAGE_COLUMN_NAME_PAGEID, pIdx);
            values.put(PageEntry.PAGE_COLUMN_NAME_PICTURE, pageArray[pIdx].getPictureAddress());
            values.put(PageEntry.PAGE_COLUMN_NAME_TEXT, pageArray[pIdx].getText());
            values.put(PageEntry.PAGE_COLUMN_NAME_TIMEVAL, pageArray[pIdx].getTime());
            values.put(PageEntry.PAGE_COLUMN_NAME_RECIPEID, recipeId);

            db.insert(PageEntry.TABLE_NAME,	"null", values);
        }
    }


    public Cursor query(String table, String selection)
    {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        if(table.equals(RecipeEntry.TABLE_NAME))
        {
            String[] projection = {RecipeEntry._ID, RecipeEntry.RECIPE_COLUMN_NAME_CATEGORY, RecipeEntry.RECIPE_COLUMN_NAME_RECIPENAME};
            String sortOrder =	RecipeEntry._ID + " ASC";
            return db.query( RecipeEntry.TABLE_NAME, projection, selection, null, null, null, sortOrder);
        }
        else if(table.equals(PageEntry.TABLE_NAME))
        {
            String[] projection = {	PageEntry._ID, PageEntry.PAGE_COLUMN_NAME_PAGEID, PageEntry.PAGE_COLUMN_NAME_PICTURE, PageEntry.PAGE_COLUMN_NAME_TEXT, PageEntry.PAGE_COLUMN_NAME_TIMEVAL,
                    PageEntry.PAGE_COLUMN_NAME_RECIPEID,};

            String sortOrder =	PageEntry.PAGE_COLUMN_NAME_PAGEID + " ASC";

            return db.query(
                    PageEntry.TABLE_NAME,
                    projection,
                    selection,
                    null, null, null, sortOrder);
        }
        else
            return null;
    }

    public void delete(String table, String selection){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.delete(table, selection, null);
    }
}
