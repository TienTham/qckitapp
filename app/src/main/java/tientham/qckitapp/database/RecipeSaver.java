package tientham.qckitapp.database;

import android.content.Context;

import tientham.qckitapp.model.Recipe;

/**
 * Created by tien on 05/03/17.
 */

public class RecipeSaver {
    private PageReaderContract contract;
    public RecipeSaver(Context context){
        contract = new PageReaderContract(context);
    }

    public void saveRecipe(Recipe recipe){
        contract.insert(recipe, true);
    }

}
