package tientham.qckitapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tien on 05/03/17.
 */

public class Page implements Parcelable{
    private Picture picture;
    private String text;
    private int timeVal;

    public Page(String pict_addr, String text, int timeval){
        this.picture = new Picture(pict_addr);
        this.text = text;
        this.timeVal = timeval;
    }

    public String getPictureAddress() {
        return picture.getAddress();
    }

    public String getText(){
        return text;
    }

    public int getTime(){
        return timeVal;
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags){
        // TODO do some stuffs with this
    }
}
