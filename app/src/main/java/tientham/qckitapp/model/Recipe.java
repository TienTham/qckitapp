package tientham.qckitapp.model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by tien on 05/03/17.
 */

public class Recipe {
    private ArrayList<Page> pageArrayList;
    private RecipeCategory recipeCategory;
    private int recipeId;
    private String recipeName;


    public Recipe(String name, Page[] pageArr, RecipeCategory category) {
        this(name, pageArr, category, -1);
    }

    public Recipe(String name, Page[] pageArr, RecipeCategory category, int id){
        pageArrayList = new ArrayList<Page>();
        for(int i=0; i<pageArr.length; i++)
            pageArrayList.add(pageArr[i]);

        this.recipeCategory = category;
        this.recipeId = id;
        this.recipeName = name;
    }
    public Recipe(String name, ArrayList<Page> pageList, RecipeCategory category){
        this.pageArrayList = pageList;
        this.recipeCategory = category;
        this.recipeId = -1;
        this.recipeName = name;
    }


    public Recipe(String name, ArrayList<Page> pageList, RecipeCategory category, int id){
        this.pageArrayList = pageList;
        this.recipeCategory = category;
        this.recipeId = id;
        this.recipeName = name;
    }

    public final Page			getPage(int pageIndex) 	{ return pageArrayList.get(pageIndex); }
    public final Page[] 		getPageArray()			{ return pageArrayList.toArray(new Page[0]); }
    public Iterator<Page> getIterator()			{ return pageArrayList.iterator(); }
    public int					getLength()				{ return pageArrayList.size(); }
    public RecipeCategory		getCategory()			{ return recipeCategory; }
    public int					getRecipeId()			{ return recipeId; }
    public String				getRecipeName()			{ return recipeName; }

}
