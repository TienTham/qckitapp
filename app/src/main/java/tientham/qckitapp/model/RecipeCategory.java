package tientham.qckitapp.model;

/**
 * Created by tien on 05/03/17.
 */

public enum RecipeCategory {
    GENERAL(0), ASIA(1), EUROPE(2);

    private int integer;
    RecipeCategory(int num){
        this.integer = num;
    }

    public int toInteger(){
        return integer;
    }

    public static RecipeCategory getCategory(int num){
        switch (num){
            case 0:
                return GENERAL;
            case 1:
                return ASIA;
            case 2:
                return EUROPE;
            default:
                return null;
        }
        //return null;
    }
}
