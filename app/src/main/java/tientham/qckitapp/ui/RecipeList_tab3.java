package tientham.qckitapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import tientham.qckitapp.R;
import tientham.qckitapp.adapter.CstmAdapter;
import tientham.qckitapp.database.RecipeLoader;
import tientham.qckitapp.model.Recipe;
import tientham.qckitapp.model.RecipeCategory;
import android.widget.AdapterView.OnItemClickListener;


/**
 * Created by tien on 05/03/17.
 */

public class RecipeList_tab3 extends Fragment {
    private ArrayList<Recipe> recipeArrayList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.list_recipe, container, false);

        ListView listView= (ListView)rootView.findViewById(R.id.foodlist);

        RecipeLoader recipeLoader = new RecipeLoader(getActivity());
        recipeArrayList = recipeLoader.getRecipeArrayList(RecipeCategory.GENERAL, false);

        CstmAdapter adapter = new CstmAdapter(getActivity(), R.layout.list_recipes_rows, recipeArrayList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int selectRecipe = recipeArrayList.get(position).getRecipeId();
                Intent i = new Intent(getActivity(), ShowRecipe.class);
                i.putExtra("selected", selectRecipe);
                i.putExtra("currentPage", 0);
                startActivity(i);
            }
        });
        return rootView;
    }

}
