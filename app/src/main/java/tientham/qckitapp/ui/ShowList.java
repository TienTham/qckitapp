package tientham.qckitapp.ui;

import android.app.ActionBar;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.app.ActionBar.Tab;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;

import java.util.Vector;

import tientham.qckitapp.R;
import tientham.qckitapp.adapter.TabsPagerAdapter;
import tientham.qckitapp.utils.ZoomOutPageTransformer;

/**
 * Created by tien on 05/03/17.
 */

public class ShowList extends FragmentActivity implements ActionBar.TabListener {
    private ViewPager viewPager;
    private TabsPagerAdapter mAdapter;
    private ActionBar actionBar;
    private String[] tabs = {"General", "Asia Food", "Europe Food"};
    private Vector<Fragment> fragmentVector;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_act);
        fragmentVector = new Vector<Fragment>();
        fragmentVector.add(new RecipeList_tab1());
        fragmentVector.add(new RecipeList_tab2());
        fragmentVector.add(new RecipeList_tab3());
        viewPager = (ViewPager) findViewById(R.id.listpager);
        actionBar = getActionBar();
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager(), fragmentVector);
        viewPager.setAdapter(mAdapter);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        for(String s: tabs){
           actionBar.addTab(actionBar.newTab().setText(s).setTabListener(this));
        }

        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener(){
            @Override
            public void onPageSelected(int position){
                // on changing the page, make respected tab selected
                actionBar.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2){
            }

            @Override
            public void onPageScrollStateChanged(int arg0){}
        });
        // fragment view goes here
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.listsearch).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        //Handle action bar item clicks here. The action bar will automatically handles clicks on the Home/UP button as you specify a parent activity in AndroidManifest
        int id = item.getItemId();
        if(id==R.id.listsearch){

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabReselected(Tab arg0, FragmentTransaction arg1){
    }

    @Override
    public void onTabSelected(Tab arg0, FragmentTransaction arg1){
        viewPager.setCurrentItem(arg0.getPosition());
    }

    @Override
    public void onTabUnselected(Tab arg0, FragmentTransaction arg1){
        // TODO Auto-generated method stuff
    }

}
