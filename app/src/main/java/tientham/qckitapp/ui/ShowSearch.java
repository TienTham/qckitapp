package tientham.qckitapp.ui;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import tientham.qckitapp.R;
import tientham.qckitapp.adapter.CstmAdapter;
import tientham.qckitapp.database.RecipeLoader;
import tientham.qckitapp.model.Recipe;
import tientham.qckitapp.model.RecipeCategory;

/**
 * Created by tien on 05/03/17.
 */

public class ShowSearch extends ActionBarActivity {
    private String query;
    private RecipeLoader recipeLoader;
    private ArrayList<Recipe> recipeArrayList;
    private android.support.v7.app.ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_act);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();

        recipeLoader = new RecipeLoader(getApplicationContext());
        recipeArrayList = new ArrayList<Recipe>();
        if(Intent.ACTION_SEARCH.equals(intent.getAction())){
            query = intent.getStringExtra(SearchManager.QUERY);
            actionBar.setTitle(query);
            Recipe[] tmp = recipeLoader.getRecipeArray(RecipeCategory.GENERAL, query, false);
            for(int i=0; i<tmp.length; i++){
                recipeArrayList.add(tmp[i]);
            }
            tmp = recipeLoader.getRecipeArray(RecipeCategory.ASIA, query, false);
            for(int i=0; i<tmp.length; i++){
                recipeArrayList.add(tmp[i]);
            }
            tmp = recipeLoader.getRecipeArray(RecipeCategory.EUROPE, query, false);
            for(int i=0; i<tmp.length; i++){
                recipeArrayList.add(tmp[i]);
            }
        }
        ListView listView = (ListView)findViewById(R.id.searchlist);
        CstmAdapter adapter = new CstmAdapter(this, R.layout.list_recipes_rows, recipeArrayList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int selectRecipe = recipeArrayList.get(position).getRecipeId();
                Intent i = new Intent(getApplicationContext(), ShowRecipe.class);
                i.putExtra("selected",selectRecipe);
                startActivity(i);
            }
        });
    }
}
